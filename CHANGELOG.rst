.. :changelog:

History
-------

0.2.1 (2020-05-11)
------------------

- Aiomysql pool support

0.1.3 (2019-12-12)
------------------

- Fix bug where aiohttp client spans weren't being finished

0.1.2 (2019-12-10)
------------------

- Remove duplicate tag for aws.request_id

0.1.1 (2019-12-10)
------------------

- Update aiobotocore imports

0.1.0 (2019-12-06)
------------------

- Initial version
