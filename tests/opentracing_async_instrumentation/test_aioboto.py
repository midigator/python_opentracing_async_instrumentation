import asyncio
import datetime
import io
import random
import string

import botocore
import mock
import pytest
from aioboto3 import Session
from aiobotocore.config import AioConfig
from botocore.exceptions import ClientError
from opentracing.ext import tags

from opentracing_async_instrumentation.client_hooks import aioboto as aioboto_hooks


@pytest.fixture
def table_name():
    return 'test_' + ''.join([random.choice(string.hexdigits) for _ in range(0, 8)])


def dynamodb_table_def(table_name):
    return dict(
        TableName=table_name,
        KeySchema=[{
            'AttributeName': 'username',
            'KeyType': 'HASH'
        }],
        AttributeDefinitions=[{
            'AttributeName': 'username',
            'AttributeType': 'S'
        }],
        ProvisionedThroughput={
            'ReadCapacityUnits': 9,
            'WriteCapacityUnits': 9
        }
    )


def moto_config():
    return {
        'aws_secret_access_key': 'xxx',
        'aws_access_key_id': 'xxx'
    }


@pytest.fixture
def region():
    return 'us-west-2'


@pytest.fixture
def signature_version():
    return 'v4'


@pytest.fixture
def config(signature_version):
    return AioConfig(
        signature_version=signature_version,
        read_timeout=5,
        connect_timeout=5
    )


@pytest.fixture
def dynamodb_resource(region, request, event_loop, dynamodb2_server, config):
    session = Session(region_name=region, **moto_config())

    async def f():
        return await session.resource('dynamodb', region_name=region, endpoint_url=dynamodb2_server, config=config).__aenter__()

    resource = event_loop.run_until_complete(f())

    yield resource

    def fin():
        event_loop.run_until_complete(resource.close())

    request.addfinalizer(fin)


@pytest.fixture
def s3_resource(region, request, event_loop, s3_server, config):
    session = Session(region_name=region, **moto_config())

    async def f():
        return await session.client('s3', region_name=region, endpoint_url=s3_server, config=config).__aenter__()

    resource = event_loop.run_until_complete(f())

    yield resource

    def fin():
        event_loop.run_until_complete(resource.close())

    request.addfinalizer(fin)


@pytest.fixture(autouse=True)
def patch_aioboto():
    aioboto_hooks.install_patches()
    try:
        yield
    finally:
        aioboto_hooks.reset_patches()


def assert_last_span(service_name, operation, tracer, response=None):
    span = tracer.recorder.get_spans()[-1]
    request_id = response and response['ResponseMetadata'].get('RequestId')
    assert span.operation_name == 'aiobotocore:client:{}:{}:{}'.format(
        service_name, service_name, operation
    )
    assert span.tags.get(tags.SPAN_KIND) == tags.SPAN_KIND_RPC_CLIENT
    assert span.tags.get(tags.COMPONENT) == 'aiobotocore'
    assert span.tags.get('aiobotocore.service_name') == service_name
    if request_id:
        assert span.tags.get('aws.request_id') == request_id


@pytest.mark.asyncio
async def test_boto3_dynamodb_with_moto(dynamodb_resource, table_name, tracer):
    await dynamodb_resource.create_table(**dynamodb_table_def(table_name))

    users = dynamodb_resource.Table(table_name)

    if asyncio.iscoroutine(users):
        users = await users

    response = await users.put_item(Item={
        'username': 'janedoe',
        'first_name': 'Jane',
        'last_name': 'Doe',
    })
    assert_last_span('dynamodb', 'PutItem', tracer, response)

    response = await users.get_item(Key={'username': 'janedoe'})
    user = response['Item']
    assert user['first_name'] == 'Jane'
    assert user['last_name'] == 'Doe'
    assert_last_span('dynamodb', 'GetItem', tracer, response)

    try:
        test = dynamodb_resource.Table('test')
        if asyncio.iscoroutine(test):
            test = await test

        await test.delete_item(Key={'username': 'janedoe'})
    except ClientError as error:
        response = error.response
    assert_last_span('dynamodb', 'DeleteItem', tracer, response)

    response = await users.creation_date_time
    assert isinstance(response, datetime.datetime)
    assert_last_span('dynamodb', 'DescribeTable', tracer)


@pytest.mark.asyncio
async def test_boto3_s3_with_moto(s3_resource, tracer):
    fileobj = io.BytesIO(b'test data')
    bucket = 'test-bucket'

    try:
        response = await s3_resource.create_bucket(Bucket=bucket)
        assert_last_span('s3', 'CreateBucket', tracer, response)
    except Exception:
        pass # sometimes python3.5's moto doesn't have the create bucket call work

    response = await s3_resource.upload_fileobj(fileobj, bucket, 'test.txt')
    assert_last_span('s3', 'CompleteMultipartUpload', tracer, response)


@mock.patch.object(aioboto_hooks, 'patcher')
def test_set_custom_patcher(default_patcher):
    patcher = mock.Mock()
    aioboto_hooks.set_patcher(patcher)

    assert aioboto_hooks.patcher is not default_patcher
    assert aioboto_hooks.patcher is patcher

    aioboto_hooks.install_patches()
    aioboto_hooks.reset_patches()

    patcher.install_patches.assert_called_once()
    patcher.reset_patches.assert_called_once()
