import asyncio
import os

import aiomysql
import logging
from unittest import mock

import opentracing
import pytest
from opentracing.ext import tags

from opentracing_async_instrumentation.client_hooks import aiomysql as aiomysql_hooks

SKIP_REASON_CONNECTION = 'MySQL is not running or cannot connect'

mysql_host = os.environ.get('MYSQL_HOST', '127.0.0.1')

@pytest.fixture(autouse=True, scope='module')
def patch_aiomysql():
    aiomysql_hooks.install_patches()
    try:
        yield
    finally:
        aiomysql_hooks.reset_patches()


def is_mysql_running():
    async def aconnect():
        try:
            async with aiomysql.connect(host=mysql_host, user='root'):
                return True
        except:
            return False

    return asyncio.get_event_loop().run_until_complete(aconnect())


def assert_span(span, operation, parent=None):
    assert span.operation_name == 'aiomysql:' + operation
    if parent:
        assert span.parent_id == parent.context.span_id
        assert span.context.trace_id == parent.context.trace_id
    else:
        assert span.parent_id is None


@pytest.mark.asyncio
@pytest.mark.skipif(not is_mysql_running(), reason=SKIP_REASON_CONNECTION)
async def test_db(tracer):
    with mock.patch('opentracing.global_tracer') as get_tracer_call:
        get_tracer_call.return_value = tracer
        assert opentracing.global_tracer() == tracer  # sanity check that patch worked

        async with aiomysql.connect(host=mysql_host, user='root') as connection:
            async with connection.cursor() as cursor:
                await cursor.execute('SELECT 1')
                rows = await cursor.fetchall()
                assert rows[0] == (1,)
                await connection.commit()

    spans = tracer.recorder.get_spans()
    assert len(spans) == 3

    print('spans' + str(spans))

    connect_span, select_span, commit_span = spans
    assert_span(connect_span, 'connect')
    assert_span(select_span, 'SELECT', tracer.active_span)
    assert_span(commit_span, 'commit', tracer.active_span)

@pytest.mark.asyncio
@pytest.mark.skipif(not is_mysql_running(), reason=SKIP_REASON_CONNECTION)
async def test_db_no_context(tracer):
    with mock.patch('opentracing.global_tracer') as get_tracer_call:
        get_tracer_call.return_value = tracer
        assert opentracing.global_tracer() == tracer  # sanity check that patch worked

        connection = await aiomysql.connect(host=mysql_host, user='root')
        cursor = await connection.cursor()

        await cursor.execute('SELECT 1')
        rows = await cursor.fetchall()
        assert rows[0] == (1,)
        await connection.commit()

    spans = tracer.recorder.get_spans()
    assert len(spans) == 3

    print('spans' + str(spans))

    connect_span, select_span, commit_span = spans
    assert_span(connect_span, 'connect')
    assert_span(select_span, 'SELECT', tracer.active_span)
    assert_span(commit_span, 'commit', tracer.active_span)

@pytest.mark.asyncio
@pytest.mark.skipif(not is_mysql_running(), reason=SKIP_REASON_CONNECTION)
async def test_db_connection_pool(tracer):
    with mock.patch('opentracing.global_tracer') as get_tracer_call:
        get_tracer_call.return_value = tracer
        assert opentracing.global_tracer() == tracer  # sanity check that patch worked

        pool = await aiomysql.create_pool(host=mysql_host, user='root')
        async with pool.acquire() as connection:
            async with connection.cursor() as cursor:
                await cursor.execute('SELECT 1')
                rows = await cursor.fetchall()
                assert rows[0] == (1,)
                await connection.commit()

    spans = tracer.recorder.get_spans()
    assert len(spans) == 3

    print('spans' + str(spans))

    connect_span, select_span, commit_span = spans
    assert_span(connect_span, 'connect')
    assert_span(select_span, 'SELECT', tracer.active_span)
    assert_span(commit_span, 'commit', tracer.active_span)

@pytest.mark.asyncio
@pytest.mark.skipif(not is_mysql_running(), reason=SKIP_REASON_CONNECTION)
async def test_db_connection_pool_no_context(tracer):
    with mock.patch('opentracing.global_tracer') as get_tracer_call:
        get_tracer_call.return_value = tracer
        assert opentracing.global_tracer() == tracer  # sanity check that patch worked

        pool = await aiomysql.create_pool(host=mysql_host, user='root')
        connection = await pool.acquire()
        cursor = await connection.cursor()

        await cursor.execute('SELECT 1')
        rows = await cursor.fetchall()
        assert rows[0] == (1,)
        await connection.commit()

    spans = tracer.recorder.get_spans()
    assert len(spans) == 3

    print('spans' + str(spans))

    connect_span, select_span, commit_span = spans
    assert_span(connect_span, 'connect')
    assert_span(select_span, 'SELECT', tracer.active_span)
    assert_span(commit_span, 'commit', tracer.active_span)
