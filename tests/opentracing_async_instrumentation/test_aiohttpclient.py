from unittest import mock

import opentracing
import pytest
from aiohttp import web
from basictracer import BasicTracer
from basictracer.recorder import InMemoryRecorder
from opentracing.scope_managers.asyncio import AsyncioScopeManager

from opentracing_async_instrumentation.client_hooks import aiohttpserver, aiohttpclient


async def handler(request):
    return web.Response(body='{:x}'.format(opentracing.global_tracer().active_span.parent_id))


@pytest.fixture(autouse=True)
def patch_aiohttp():
    aiohttpclient.patcher.install_patches()
    try:
        yield
    finally:
        aiohttpclient.patcher.reset_patches()


@pytest.fixture
def tracer():
    t = BasicTracer(
        recorder=InMemoryRecorder(),
        scope_manager=AsyncioScopeManager(),
    )
    t.register_required_propagators()
    return t


async def test_aiohttp(aiohttp_client, tracer):
    app = web.Application()
    app.router.add_get('/', handler)
    app.middlewares.append(aiohttpserver.enable_tracing)
    client = await aiohttp_client(app)

    with mock.patch('opentracing.global_tracer') as get_tracer_call:
        get_tracer_call.return_value = tracer
        assert opentracing.global_tracer() == tracer  # sanity check that patch worked

        span = tracer.start_span('test')
        span_id = None

        def resp_handler(response, span):
            nonlocal span_id
            span_id = '{:x}'.format(span.context.span_id)

        aiohttpclient.patcher.set_response_handler_hook(resp_handler)

        with span:
            response = await client.get('/')

        span.finish()

        assert response.status == 200
        assert (await response.text()) == span_id

        assert span.tracer.recorder.spans[1].operation_name == 'GET'
        assert span.tracer.recorder.spans[1].tags['span.kind'] == 'client'
        assert span.tracer.recorder.spans[1].duration is not None
